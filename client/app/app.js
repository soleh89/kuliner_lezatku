'use strict';

angular.module('kulinerLezatkuApp', [
  'kulinerLezatkuApp.auth',
  'kulinerLezatkuApp.admin',
  'kulinerLezatkuApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'angularMoment',
  'ui-notification'
])
  .config(function($urlRouterProvider, $locationProvider, NotificationProvider) {
    NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'right',
      positionY: 'bottom'
    });

    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
