'use strict';

(function() {

class ItemController {

  constructor($http, $scope, socket) {
    this.awesomeItems = [];
    this.loading = true;
    this.totalItems = 0;
    this.currentPage = 1;

    $http.get('/api/items').then(response => {
      this.loading = false;
      this.awesomeItems = response.data;
      socket.syncUpdates('item', this.awesomeItems);
      this.totalItems = this.awesomeItems.length;
    });
  }

}

angular.module('kulinerLezatkuApp')
  .controller('ItemController', ItemController);

})();
