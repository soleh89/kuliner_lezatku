'use strict';

angular.module('kulinerLezatkuApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'app/main/about.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('how_to_use', {
        url: '/how_to_use',
        templateUrl: 'app/main/how_to_use.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('pricing', {
        url: '/pricing',
        templateUrl: 'app/main/pricing.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('contact_us', {
        url: '/contact_us',
        templateUrl: 'app/main/contact_us.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('faq', {
        url: '/faq',
        templateUrl: 'app/main/faq.html',
        controller: 'MainController',
        controllerAs: 'main'
      }).state('profile', {
        url: '/profile',
        templateUrl: 'app/main/profile.html',
        controller: 'MainController',
        controllerAs: 'main'
      }).state('preview', {
        url: '/items/:id/preview',
        templateUrl: 'app/main/preview.html',
        params: { item: null},
        controller: 'MainController',
        controllerAs: 'main'
      }).state('items', {
        url: '/items',
        templateUrl: 'app/main/items.html',
        controller: 'ItemController',
        controllerAs: 'item'
      });
  });
