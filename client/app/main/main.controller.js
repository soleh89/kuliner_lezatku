'use strict';

(function() {

class MainController {

  constructor($http, $scope, socket, Auth, $location, $stateParams, Notification) {
    this.$http = $http;
    this.awesomeItems = [];
    this.userItems = [];
    this.myProfile = true;
    this.loading = false;
    this.loading2 = false;
    this.myParams = null;
    this.comments = [];
    this.notif = Notification;
    this.statusNewItem = false;
    this.socket = socket;
   
    this.isLoggedIn = Auth.isLoggedIn;
    this.getCurrentUser = Auth.getCurrentUser;
    
    if(!this.isLoggedIn()){
      if($location.path() == "/") {
        this.loading = true;
        $http.get('/api/items/top_kuliner').then(response => {
          this.loading = false;
          this.awesomeItems = response.data;
          socket.syncUpdates('item', this.awesomeItems);
        });
      }

      this.myParams = $stateParams.item; 
    }else{
      if($location.path() == "/") {
        this.loading = true;
        $http.get('/api/items/' + this.getCurrentUser()._id + '/own_item').then(response => {
          this.loading = false;
          this.currentPage = 1;        
          this.userItems = response.data;
          socket.syncUpdates('item', this.userItems);
          this.totalItems = this.userItems.length;
        });
      }
    }    

    // $scope.$on('$destroy', function() {
    //   socket.unsyncUpdates('item');
    // });
  }

  editUser() {
    this.myProfile = false;
  }

  cancelUser() {
    this.myProfile = true;
  }

  updateUser(user) {
    this.loading = true;
    this.$http.put('/api/users/' + user._id, user).then(response => {
      this.loading = false;
      this.myProfile = true;
      this.notif("Update profile successfully");
    }).catch(function(e){
      this.notif("Update profile failed : " + e);
    });
  }

  newItem() {
    this.statusNewItem = true;
  }

  cancelItem() {
    this.statusNewItem = false;
  }

  addNewItem(form, user) {
    this.loading2 = true;
    form._user = user._id;
   
    this.$http.post('/api/items', form).then(response => {
      this.loading2 = false;
      this.statusNewItem = false;
      this.loading = true;
      this.$http.get('/api/items/' + user._id + '/own_item').then(response => {
        this.loading = false;
        this.currentPage = 1;        
        this.userItems = response.data;
        this.socket.syncUpdates('item', this.userItems);
        this.totalItems = this.userItems.length;
        this.item.name = "";
        this.item.description = "";
        this.item.image = "";
        this.item.price = "";
      });

      // this.notif("Create item successfully");
    }).catch(function(e){
      // console.log("error : " + e);
      // this.notif("Create item failed : " + e);
    });
  }

  // addItem() {
  //   if (this.newItem) {
  //     this.$http.post('/api/items', { name: this.newItem });
  //     this.newItem = '';
  //   }
  // }

  // deleteItem(item) {
  //   this.$http.delete('/api/items/' + item._id);
  // }

}

angular.module('kulinerLezatkuApp')
  .controller('MainController', MainController);

})();
