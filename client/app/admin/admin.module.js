'use strict';

angular.module('kulinerLezatkuApp.admin', [
  'kulinerLezatkuApp.auth',
  'ui.router'
]);
