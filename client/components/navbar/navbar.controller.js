'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'home': 'Kuliner Lezatku',
    'about': 'About',  
    'pricing': 'Pricing',
    'contact_us': 'Contact Us',
    'faq': 'Faq',
    'profile': 'Profile',
    'state_main': 'main',
    'state_about': 'about',
    'state_pricing': 'pricing',
    'state_contact_us': 'contact_us',
    'state_faq': 'faq',
    'state_profile': 'profile'
  }];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

angular.module('kulinerLezatkuApp')
  .controller('NavbarController', NavbarController);
