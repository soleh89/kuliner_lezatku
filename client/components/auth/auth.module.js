'use strict';

angular.module('kulinerLezatkuApp.auth', [
  'kulinerLezatkuApp.constants',
  'kulinerLezatkuApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
