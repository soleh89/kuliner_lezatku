/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/items              ->  index
 * POST    /api/items              ->  create
 * GET     /api/items/:id          ->  show
 * PUT     /api/items/:id          ->  update
 * DELETE  /api/items/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Item from './item.model';

var cloudinary = require('cloudinary')

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Items
export function index(req, res) {
  // Item.findAsync()
  //   .then(respondWithResult(res))
  //   .catch(handleError(res));
  
  Item.find({})
    .populate("_user", "name email address phone_number picture fb_url twitter_url instagram_url").sort({created_at: 'descending', name: 'ascending'})
    .execAsync()
    .then(function(items) {
        res.json(items);
    })
    .catch(function(err) {
        res.send(err);
    });
}

// Gets a top of Items
export function top_kuliner(req, res) {
  Item.find({})
    .populate("_user", "name email address phone_number picture fb_url twitter_url instagram_url").sort({score: 'descending', name: 'ascending'})
    .execAsync()
    .then(function(items) {
        res.json(items.slice(0, 6));
    })
    .catch(function(err) {
        res.send(err);
    });
}

// Gets a single Item from the DB
export function show(req, res) {
  Item.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Item per user from the DB
export function own_item(req, res) {
  Item.findAsync({_user: req.params.user_id})
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Item in the DB
export function create(req, res) {
  cloudinary.config({ 
    cloud_name: 'drvaepmvp', 
    api_key: '921377317694525', 
    api_secret: 'a2lD0n_PopPLIbVT6dzPu5kZ_Gk' 
  });

  cloudinary.uploader.upload(req.body.image, function(result) { 
    req.body.image = result.url;
    Item.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
  });
  // Item.createAsync(req.body)
  //   .then(respondWithResult(res, 201))
  //   .catch(handleError(res));
}

// Updates an existing Item in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Item.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Item from the DB
export function destroy(req, res) {
  Item.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
