'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var ItemSchema = new mongoose.Schema({
  name: {
    type: String,
    default: '-'
  },
  description: {
    type: String,
    default: '-'
  },
  image: String,
  price: {
    type: Number,
    default: 0.0
  },
  score: {
    type: Number,
    default: 0.0
  },
  active: Boolean,
  created_at: { type: Date, required: true, default: Date.now },
  updated_at: { type: Date, required: true, default: Date.now },
  _user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

export default mongoose.model('Item', ItemSchema);
