'use strict';

var express = require('express');
var controller = require('./item.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/top_kuliner', controller.top_kuliner);
router.get('/:id', controller.show);
router.get('/:user_id/own_item', auth.isAuthenticated(), controller.own_item);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);
router.post('/', auth.isAuthenticated(), controller.create);

module.exports = router;
