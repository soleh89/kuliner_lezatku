/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Item from '../api/item/item.model';
import User from '../api/user/user.model';

User.find({}).removeAsync()
  .then(() => {
    User.createAsync({
      _id: '56d347f515a4e0ad367b4435',
      provider: 'local',
      name: 'Maknyus',
      email: 'test@example.com',
      password: 'test',
      address: 'jl. sadarmanah no 76 bandung',
      phone_number: '0226638821',
      instagram_url: 'maknyus_ig_official'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
    .then(() => {
      console.log('finished populating users');
    });
  });

Item.find({}).removeAsync()
  .then(() => {        
    Item.createAsync({
      name: 'Ayam Bakar Madu',
      description: 'Ayam bakar madu super enak, rasa yang manis di padu gurih dari bumbu rempah2 pilihan.',
      price: 17000,
      _user: "56d347f515a4e0ad367b4435",
      score: 8
    }, {
      name: 'Cumi Asam Manis',
      description: 'Cumi asam manis, di pilih dari bahan2 berkualitas.',
      price: 25000,
      _user: "56d347f515a4e0ad367b4435",
      score: 9
    }, {
      name: 'Baso Urat',
      description: 'Baso sapi enak, sehat, dan harga mahasiswa.',
      price: 10000,
      _user: "56d347f515a4e0ad367b4435",
      score: 5
    }, {
      name: 'Sate Kambing',
      description: 'Sate kambing dengan daging pilihan, enak, bumbu khas dan di jamin murah.',
      price: 15000,
      _user: "56d347f515a4e0ad367b4435",
      score: 8
    }).then(() => {
      console.log('finished populating items');
    });
  });